
![image](Images/organization_logo.png "image name")
## Introduction

In this lab, we will learn how to use the Keras library to build models for classificaiton problems. We will use the popular MNIST dataset, a dataset of images, for a change. 

The <strong>MNIST database</strong>, short for Modified National Institute of Standards and Technology database, is a large database of handwritten digits that is commonly used for training various image processing systems. The database is also widely used for training and testing in the field of machine learning.
    
The MNIST database contains 60,000 training images and 10,000 testing images of digits written by high school students and employees of the United States Census Bureau.

Also, this way, will get to compare how conventional neural networks compare to convolutional neural networks, that we will build in the next module.

## Table of Contains



1. <a href="">Import Keras and Packages</a>
2. <a href="">Build a Neural Network</a>
3. <a href="">Train and Test the Network</a>

## Import Keras and Packages

Let's start by importing Keras and some of its modules.

``` python
import keras
from keras.models import Sequential
from keras.layers import Dense
from keras.utils import to_categorical
```

Since we are dealing we images, let's also import the Matplotlib scripting layer in order to view the images.

`import matplotlib.pyplot as plt`

So, let's load the MNIST dataset from the Keras library. The dataset is readily divided into a training set and a test set.

```
# import the data
from keras.datasets import mnist

# read the data
(X_train, y_train), (X_test, y_test) = mnist.load_data()
```
Let's confirm the number of images in each set. According to the dataset's documentation, we should have 60000 images in X_train and 10000 images in the X_test.

