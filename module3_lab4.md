# Hands-on-lab: Adding Discovery to the Chatbot

# Objective:
- How to integrate the **Watson Discovery** collection with the chatbot.

In the previous lab, we defined a basic chatbot that can offer hard-coded responses to some chitchat interactions and intents. However, course catalog is too big for us to provide specific course recommendations directly in the response section of a node.

1. Open a terminal window by using the menu in the editor: Terminal > New Terminal.
<img src="images/new_terminal.png"  style="margin-top:10px; margin-bottom:10px"/>

2. Go to the project home directory.

    ```
    cd /home/project
    ```
    {: codeblock}

3. Run the following command to Git clone the project directory from the below clone URL:

    ```
    git clone https://github.com/ibm-developer-skills-network/ncehu-AppliedAI_6_Module4.git
    ```
    {: codeblock}

    At this point, you should see a project **ncehu-AppliedAI_6_Module4** listed in the left sidebar of Theia IDE. 
    Click on the ncehu-AppliedAI_6_Module4 folder, you will see a file called **serverless.yml**.

    In **serverless.yml** replace the value of **YOUR\_USERNAME** to &quot; **apikey**&quot;, **YOUR_DISCOVERY_PASSWORD**  to the  **API key**  from your Discovery service. Similarly, replace the values of  **ENVIRONMENT_ID** and **COLLECTION_ID**  with the values obtained earlier above. Replace the value of **YOUR_DISCOVERY_URL** with the  **url** we obtained earlier. **URL** is location specific.

    > Make sure to **press control + S**(Windows) **or command + S**(MacOS) to save the configuration.

4. If you want, spend some time investigating the **CourseAdvisor.js** code. In particular, have a look at line 97.

    The description field of our collection was enriched with Watson insights and we can query on **concepts** and **keyword**. Since the **keyword extraction** enrichment identifies content typically used when indexing data, generating tag clouds, or searching, we will run a query on **keyword** to recommend a course based on the user input.

5. Change to project directory
    ```
    cd ncehu-AppliedAI_6_Module4
    ```
    {: codeblock}

6. Login to the ibmcloud with your email and enter your password.
    ```
    ibmcloud login --no-region -u YOUR_IBMCLOUD_EMAIL -p YOUR_IBMCLOUD_PASSWORD
    ```
    {: codeblock}

    **Note**: If you get the following **error** message, &quot; **FAILED Unable to authenticate. You are using a federated user ID"** , you can either 

     - Create an IBM Cloud account with your personal email (not an ibm.com) and try again or 

    - Create an API key using your federated IBM Cloud account [https://cloud.ibm.com/docs/iam?topic=iam-userapikey#create\_user\_key](https://cloud.ibm.com/docs/iam?topic=iam-userapikey#create_user_key) and  **copy the following lines below** and replace **YOUR_APIKEY** with your actual **API Key**.

    ```
    ibmcloud login --apikey YOUR_APIKEY
    ```
    {: codeblock}

7. We will use the **name of org** under **Name** and **Region** in the next cell.
    ```
    ibmcloud account orgs
    ```
    {: codeblock}

8. Replace **REGION** with **region** and **Name_OF_ORG** with **the name of org** you obtained above. 

    ```
    ibmcloud target --cf-api 'https://api.REGION.cf.cloud.ibm.com' -r REGION -o NAME_OF_ORG
    ```
    {: codeblock}

9. Create account space if it doesn't already exist.
    ```
    ibmcloud account space-create lab4
    ```
    {: codeblock}

10. Target the account space you just created.

    ```
    ibmcloud target -s lab4
    ```
    {: codeblock}

10. Install node package `serveless`
    ```
    export npm_config_loglevel=silent
    npm install serverless@1.51.0
    ```
    {: codeblock}

11. Replace **REGION** with **region** you obtained above. 
    ```
    ibmcloud fn list --apihost REGION.functions.cloud.ibm.com
    ```
    {: codeblock}

12. Deploy serverless
    ```
    serverless deploy
    ```
    {: codeblock}

14. List function now
    ```
    ibmcloud fn list
    ```
    {: codeblock}

    ## Author(s)
[Antonio Cangiano](https://www.linkedin.com/in/antoniocangiano/)


## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2021-07-07 | 2.0 | Shubham | Migrated Lab to Markdown and added to course repo in GitLab |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
