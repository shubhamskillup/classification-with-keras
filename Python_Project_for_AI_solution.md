# Final Assignment Solution

The solution is provided for Task1, Task2, and Task6 which involves coding. For other tasks please follow the Final assignment instructions.
Also, before you start ensure you have provisioned an instance of the Watson Language Translator service and have API information available.


## Task1: Write a function that translates English text to French in translator.py

1. Till step 8 please follow the instructions given in the Assignment as it is.

2. Now, for step9 create **translatorinstance** function.

```
def translatorinstance():
    """
    Translator instance
    """
    authenticator = IAMAuthenticator(apikey)
    language_translator = LanguageTranslatorV3(
    version='2018-05-01',
    authenticator=authenticator
    )
    language_translator.set_service_url(url)
    return language_translator
```
3. Create function **englishToFrench** which takes in the englishText as a string argument, in **translator.py**.
```
def englishtofrench(englishtext):
    """
    This function translates english to french
    """
    language_translator = translatorinstance()
    translation_response = language_translator.translate(
    text=englishtext, model_id='en-fr').get_result()
    frenchtext = translation_response.get("translations")[0].get("translation")
    return frenchtext

```
4. Create function **frenchToEnglish** which takes in the frenchText as a string argument, in **translator.py**. Use the instance of the Language Translator you created in step 2 to translate the text input in French to English and return the English text.

```
def frenchtoenglish(frenchtext):
    """
    This function translates french to english
    """
    language_translator = translatorinstance()
    translation_response = language_translator.translate(
    text=frenchtext,model_id='fr-en').get_result()
    englishext = translation_response.get("translations")[0].get("translation")
    return englishext

```
## Task 2: Write the unit tests for English to French translator and French to English translator function  in tests.py


```
import unittest
from translator import englishtofrench, frenchtoenglish

class TestTranslator(unittest.TestCase):
    
    def test_null(self):
        self.assertRaises(ValueError, englishtofrench, None)
        self.assertRaises(ValueError, frenchtoenglish, None)

    def test_englishtofrench(self):
        self.assertEqual(englishtofrench("Hello"), "Bonjour")
        self.assertEqual(englishtofrench("How are you?"), "Comment es-tu?")
        self.assertNotEqual(englishtofrench("Hello"), "Hallo")

    def test_frenchToEnglish(self):
        self.assertEqual(frenchtoenglish("Bonjour"), "Hello")
        self.assertEqual(frenchtoenglish("Comment es-tu?"), "How are you?")
        self.assertNotEqual(frenchtoenglish("Bonjour"), "Hallo")


unittest.main()
```
## Task 6: Import the package into server.py and create flask end points

Code of **server.py** file

```
from machinetranslation import translator
from flask import Flask, render_template, request
import json

app = Flask("Web Translator")

@app.route("/englishToFrench")
def englishToFrench():
    textToTranslate = request.args.get('textToTranslate')
    return translator.englishtofrench(textToTranslate)

@app.route("/frenchToEnglish")
def frenchToEnglish():
    textToTranslate = request.args.get('textToTranslate')
    return translator.frenchtoenglish(textToTranslate)

@app.route("/")
def renderIndexPage():
    return render_template("index.html")

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)

```


## Thank you!

